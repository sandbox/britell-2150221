<?php

  /**
   * @file
   * This module implements a navigation tree based on query information
   * from the quickdraw base module.
   */

  /**
   * Implements hook_menu().
   */
  function quickdraw_navmenu_menu() {
    $items = array();
    // returns the children of the node given
    $items['quickdraw/navmenu'] = array(
      'title' => 'quickdraw_navmenu AJAX',
      'type' => MENU_CALLBACK,
      'page callback' => 'quickdraw_navmenu_ajax',
      'access arguments' => array('access content'),
    );
    // additional url to return root + children
    $items['quickdraw/navmenu/root'] = array(
      'title' => 'quickdraw_navmenu AJAX root',
      'type' => MENU_CALLBACK,
      'page callback' => 'quickdraw_navmenu_ajax_root',
      'access arguments' => array('access content'),
    );
    $items['quickdraw/error'] = array(
      'title' => 'quickdraw_navmenu AJAX',
      'type' => MENU_CALLBACK,
      'page callback' => 'quickdraw_navmenu_error',
      'access arguments' => array('access content'),
    );

    return $items;
  }

  /**
   * Callback to display an error message in the navbar.
   */
  function quickdraw_navmenu_error() {
    print 'There was an error acquiring the node ID.';
  }

  /**
   * Generate the navmenu for a given html id'd node.
   *
   * This is the main function that will produce the entire navmenu
   * with the given html id used as the root node in the tree.
   *
   * @param html_id The html id of node to use as root.
   */
  function quickdraw_navmenu_ajax($html_id) {
    $nid = quickdraw_navmenu_get_node_id_from_html_id($html_id);
    print quickdraw_navmenu_get_children($html_id, $nid);
  }

  /**
   * Print the html markup of the root of the tree with the given
   * html_id
   *
   * @param html_id The html id of the node to use as the root node
   */
  function quickdraw_navmenu_ajax_root($html_id) {
    print quickdraw_navmenu_ajax_root_value($html_id);
  }

  /**
   * Same as above, but includes the root node in the tree instead
   * of simply collecting the children.
   *
   * @param html_id The html ID of the root node.
   */
  function quickdraw_navmenu_ajax_root_value($html_id) {
    $nid = quickdraw_navmenu_get_node_id_from_html_id($html_id);
    $node = node_load($nid);
    $rid = quickdraw_navmenu_get_root_id($node);
    $root = node_load($rid);
    return quickdraw_navmenu_make_node_li(NULL, NULL,
      $rid, $root->title,
      node_type_get_name($root->type));
  }

  /**
   * Extract the node id from the html page id for a node.
   *
   * HTML id values need to be unique, so we use html ids composed
   * of concatenations of the parent ids and the actual node id.
   *
   * @param html_id The html id to extract from.
   * @return The node id, which is stored at the end of the html id.
   */
  function quickdraw_navmenu_get_node_id_from_html_id($html_id) {
    return end(explode('n', $html_id));
  }

  /**
   * Get HTML text of the clustered children of a node.
   *
   * @param parent_id HTML id for the parent (aka nid?)
   * @param nid The parent of the clusters for whom we are searching.
   * @return HTML text for a ul-tree of the cluster children.
   */
  function quickdraw_navmenu_get_clusters($parent_id, $nid) {
    $html = '';
    // ask the db for the cluster children
    $clusters = quickdraw_navmenu_query_clusters($nid);
    foreach ($clusters as $cluster) {
      // sequentially get the li-tree part markup for each cluster
      $html .= quickdraw_navmenu_make_cluster_li($parent_id, $nid, $cluster);
    }
    return $html;
  }

  /**
   * Generate the li element for a given cluster.
   *
   * @param cluster The cluster we are generating the elemnt for.
   * @param nid The node id of the cluster's parent.
   * @param parent_id The html id of the cluster's parent?
   */
  function quickdraw_navmenu_make_cluster_li($parent_id, $nid, $cluster) {
    $cluster_html_id = $parent_id . 'r' . $cluster->cluster_id;
    return '<li id="' . $cluster_html_id
      . '"><label><span class="aggtype">'
      . $cluster->cluster_name
      . ' (' . quickdraw_navmenu_count_cluster_children($nid, $cluster)
      . ')</span></label><ul>'
      . quickdraw_navmenu_make_cluster_children($cluster_html_id, $nid, $cluster)
      . '</ul></li>';
  }

  /**
   * Currently unused.
   */
  function quickdraw_navmenu_make_cluster_children($parent_id, $nid, $cluster) {
    $html = '';
    $clustered = quickdraw_navmenu_get_clustered_nodes($nid, $cluster->cluster_id);
    foreach ($clustered as $part) {
      if ($part->title == '') {
        // fixme: there don't seem to be any subclusters in our data
        $html .= quickdraw_navmenu_get_clusters($parent_id, $nid);
      }
      else {
        $html .= quickdraw_navmenu_make_node_li(
          $parent_id,
          $cluster->cluster_id,
          $part->part,
          $part->title,
          $part->relationship_type
        );
      }
    }
    return $html;
  }

  /**
   * Generate an li element for a node.
   *
   * @param nid The node id of the node.
   * @param parent_id The html id of the parent.
   * @param relationship Unused...?
   * @param title The display name for the node.
   * @param type Unused?
   */
  function quickdraw_navmenu_make_node_li($parent_id, $relationship, $nid, $title, $type) {
    // Produce an html ID for this node
    $li_id = quickdraw_navmenu_make_li_id($parent_id, $relationship, $nid);
    // If the node has children, mark it closed.
    // otherwise it is a leaf
    $class = quickdraw_navmenu_node_has_children($nid)? "jstree-closed" : "jstree-leaf";
    // An li element for a node has an html id which we produced above.
    // A class based on whether it is a leaf or not.
    // ...social nav info...
    // A link to itself, based on its node id.
    // Some text for its name, type, etc.
    return "<li id='$li_id' class='$class'>"
         . quickdraw_navmenu_social_nav($nid)
         . "<label><a href='/node/$nid'>"
         . quickdraw_navmenu_make_li_span($nid, $type, $title)
         . '</a></label>'
         . '</li>';
  }

  /**
   * Determine whether a node has any children.
   *
   * @return true if the node has at least 1 child, false otherwise.
   */
  function quickdraw_navmenu_node_has_children($nid) {
    return quickdraw_navmenu_node_has_clustered_children($nid)
        || quickdraw_navmenu_node_has_unclustered_children($nid);
  }

  /**
   * Determine whether a node has any children who are clusters.
   *
   * @param nid The node to check.
   * @return true if the node has at least 1 child that is a cluster,
   *         false otherwise.
   */
  function quickdraw_navmenu_node_has_clustered_children($nid) {
    /*$clusters = quickdraw_navmenu_query_clusters($nid);
    foreach ($clusters as $cluster) {
      if (quickdraw_navmenu_count_cluster_children($nid, $cluster) > 0) {
        return true;
      }
    }*/
    return FALSE;
  }

  /**
   * Determine if a node has unclustered children.
   *
   * @param nid The node id of the node to check.
   * @return true if the node has non-clustered children; false otherwise.
   */
  function quickdraw_navmenu_node_has_unclustered_children($nid) {
    foreach (quickdraw_query($nid) as $unused) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Produce HTML text for the tree structure defined by a node's children.
   *
   * @param parent_id The HTML id of the parent of the node.
   * @param nid The node id of the node we working from.
   * @return The HTML markup for the node's children.
   */
  function quickdraw_navmenu_get_children($parent_id, $nid) {
    $html = '';
    $children = quickdraw_query($nid);
    $type_count = array();
    // make li items for each child
    foreach ($children as $child) {
      // handle counting
      if (!isset($type_count[$child['type']])) {
        $type_count[$child['type']] = 1;
      }
      else {
        $type_count[$child['type']] += 1;
      }
      // handle based on type for clustering, etc.
      $html .= quickdraw_navmenu_make_node_li($nid, NULL, $child['nid'], $child['title'], $child['type'] . ' ' . $type_count[$child['type']]);
    }
    return $html;
  }

  /**
   * Implements hook_block().
   *
   * Registers the blocks for this module.
   */
  function quickdraw_navmenu_block_info() {
    $blocks['quickdraw_navmenu'] = array(
      'info' => t('QD Navigation Menu'),
      'cache' => DRUPAL_CACHE_PER_ROLE,
    );
    $blocks['quickdraw_navmenu_preview'] = array(
      'info' => t('QD Navigation Menu Previewer'),
      'cache' => DRUPAL_CACHE_PER_ROLE,
    );
    return $blocks;
  }

  /**
   * Implements hook_block_view().
   *
   * Defines how the blocks are displayed.
   */
  function quickdraw_navmenu_block_view($delta = '') {
    $block = array();

    switch ($delta) {
      case "quickdraw_navmenu":
        $block['subject'] = t('Navigation');
        $block['content'] = quickdraw_navmenu_generate_ajax_block();
      break;
      case "quickdraw_navmenu_preview":
        $block['subject'] = t('Navigation Preview');
        $block['content'] = _quickdraw_navmenu_generate_preview_block();
      break;
    }

    return $block;
  }

  /**
   * Generate the html markup for the preview block.
   *
   * @return  The html markup for the preview block.
   */
  function _quickdraw_navmenu_generate_preview_block() {
    quickdraw_navmenu_set_headers();
    $html = '<input id="preview_root" type="text" size="20" />';
    $html .= '<input type="button" value="Preview" onclick="quickdraw_navmenu_get_initial_preview_tree(document.getElementById(\'preview_root\').value)" />';
    $html .= '<div id="quickdraw_navmenu_ajax_preview_tree">Enter a node ID to get started.</div>';
    return $html;
  }

  /**
   * Create HTML markup for inital navmenu div.
   */
  function quickdraw_navmenu_generate_ajax_block() {
    if (quickdraw_navmenu_page_is_node()) {
      quickdraw_navmenu_set_headers();

      $nid = arg(1);
      $node = node_load($nid);

      $root_id = quickdraw_navmenu_get_root_id($node);
      $root = node_load($root_id);

      return '<div id="quickdraw_navmenu_ajax_tree" style="overflow:auto;"><ul></ul></div>';
    }
    else {
      return '';
    }
  }

  /**
   * Determine whether a node's type is supported by the navbar.
   *
   * @return true if the type is supported, false otherwise.
   */
  function quickdraw_navmenu_node_is_supported_type($node) {
    return quickdraw_navmenu_has_children($node->nid)
       || (quickdraw_navmenu_get_root_id($node) != $node->nid);
  }

  /**
   * Determine if a given node is a child of another node.
   *
   * @param root_id The 'parent' to check.
   * @param node_id The 'child' to check.
   * @return true if the node with id root_id is the parent of the node
   *         with id node_id.
   */
  function quickdraw_navmenu_root_is_ancestor_of_node($root_id, $node_id) {
    $results = quickdraw_query($root_id);
    foreach ($results as $child) {
      if ($child['nid'] == $node_id) {
        return TRUE;
      }
      elseif (quickdraw_navmenu_root_is_ancestor_of_node($child['nid'], $node_id)) {
        return TRUE;
      }
    }
    return FALSE;
  }

  /**
   * Determine whether the page we are on is a node's.
   *
   * @return true if the page url appears to be that of a node,
   *         false otherwise.
   */
  function quickdraw_navmenu_page_is_node() {
    // urls for nodes have their first arg as the word "node"
    // and the second a node id number
    return arg(0) == "node" && is_numeric(arg(1));
  }

  /**
   * Get the node id of the root node in the tree we are displaying.
   *
   * @param node A node in the tree.
   * @return The node id of the root of the tree.
   */
  function quickdraw_navmenu_get_root_id($node) {
    $root = '';
    // collection nodes are always the roots of their own trees
    if ($node->type == 'collection_page') {
      $root = $node->nid;
      setcookie("quickdraw_navmenu_root", $node->nid);
    }
    else {
      //check for a nav tree root in the cookie, if it exists use it
      //otherwise set the root to the current nodeid
      // Cookie keeps track of a tree we have been navigating from.
      // If we jump in from an unknown place, we set the cookie.
      if (!array_key_exists('quickdraw_navmenu_root', $_COOKIE)) {
        $root = NULL;
      }
      else {
        $root = $_COOKIE['quickdraw_navmenu_root'];
      }

      // need to check for ancestor, not just parent!
      if (!is_numeric($root) || !quickdraw_navmenu_root_is_ancestor_of_node($root, $node->nid)) {
        $root = $node->nid;
        setcookie("quickdraw_navmenu_root", $node->nid);
      }
    }
    return $root;
  }

  /**
   * Link in the scripts that the navmenu module requires, and add the styles
   * the tree uses.
   */
  function quickdraw_navmenu_set_headers() {
    //add javascript tree to the html
    $js_path = '/' . drupal_get_path('module', 'quickdraw_navmenu');
    drupal_add_html_head(
      array(
        '#type' => 'markup',
        '#markup' => '
          <script src="' . $js_path . '/jquery.min.js" type="text/javascript"></script>
          <script src="' . $js_path . '/jquery.cookie.js" type="text/javascript"></script>
          <script src="' . $js_path . '/jquery.jstree.js" type="text/javascript"></script>
          <script src="' . $js_path . '/inline_script.js" type="text/javascript"></script>
          <style>
           .mattype { color: black; }
           .jstree label { display: inline; }
           .jstree-closed > label > a > ins { display: none !important;}
           .jstree-open > label > a > ins { display: none !important; }
           .jstree-leaf > label > a > ins { display: none !important; }
           .jstree li a ins { display:none !important; }
           /* found online, makes wrapping seem to work well */
           .jstree a {
                       white-space: normal !important; height: auto;
                       margin-right: 20px !important;
                     }
           .jstree li > ins { vertical-align: top; }
          </style>',
      ),
      'jquery-tmpl'
    );
  }

  /**
   * Generates the li element for a node.
   *
   * @param parent_id The parent's html ID.
   * @param relationship Unused.
   * @param nid The node id of the node that will use this element.
   * @param title The human readable title of the node.
   * @param type The kind of display node.
   * @param recurse Whether to recursively continue building the tree for
   *                all children.
   */
  function quickdraw_navmenu_make_li($parent_id, $relationship, $nid, $title, $type, $recurse) {
    $li_id = quickdraw_navmenu_make_li_id($parent_id, $relationship, $nid);
    $html = '<li id="' . $li_id . '">'
          . quickdraw_navmenu_social_nav($nid)
          . '<label><a href="/node/' . $nid . '">'
          . quickdraw_navmenu_make_li_span($nid, $type, $title)
          . '</a></label>';
    if ($recurse) {
      $html .= quickdraw_navmenu_generate_list($li_id, $nid);
    }
    return $html . '</li>';
  }

  /**
   * Create the HTML id for a node.
   *
   * @param parent_id The parent's html id.
   * @param relationship Unused...?
   * @param nid The id of the node.
   * @return The HTML id of the node with the given positioning as a string.
   */
  function quickdraw_navmenu_make_li_id($parent_id, $relationship, $nid) {
    return $parent_id
         //. (empty($relationship)? '' : ('r' . $relationship))
         . 'n' . $nid;
  }

  /**
   * Extract the root node id from an html ID.
   *
   * @param html_id The id to extract from.
   * @return The root note id indicated by the html ID.
   */
  function quickdraw_navmenu_get_root_id_from_html_id($html_id) {
    $v = explode('n', $html_id);
    $v = $v[1];
    return $v;
  }

  /**
   * Generate the title text span for an li element.
   *
   * @param nid The node we are writing this for.
   * @param type The type of the node. May be omitted.
   * @param title The title text (name of the node).
   * @return A span element containing the display name.
   */
  function quickdraw_navmenu_make_li_span($nid, $type, $title) {
    if (isset($type)) {
      return '<span class="mattype">' .
               '<span class="u' . $nid . '">' .
                 $type . ': ' .
               '</span>' .
             '</span>' .
             $title;
    }
    else {
      return '<span class="u' . $nid . '">' .
               $title .
             '</span>';
    }
  }

  /**
   * Query the database for the unclustered children of a node.
   *
   * @param nid The node whose children we are looking at.
   * @return A query object containing the node's unclustered children.
   */
  function quickdraw_navmenu_query_unclustered_children($nid) {
    return quickdraw_query($nid);
  }

  /**
   * Generate the unordered list structure of a node.
   *
   * @param parent_html_id The html id of the node's parent.
   * @param nid The node id of the node.
   * @return HTML markup of a unordered list that represents the node (and its children).
   */
  function quickdraw_navmenu_generate_list($parent_html_id, $nid) {
    $list = '<ul>';

    //first get cluster types if they exist
    //$list .= quickdraw_navmenu_make_clusters($parent_html_id, $nid);
    //then get non-clustered children
    $result = quickdraw_query($nid);
    foreach ($result as $child) {
      $list .= quickdraw_navmenu_make_li($parent_html_id, NULL, $child['id'], $child['title'], $child['rel'], TRUE);
    }

    if ($list == '<ul>') {
      return '';
    }
    else {
      return $list . '</ul>';
    }
  }

  /**
   * Make a database query to get the clustered children of some node.
   *
   * @param nid The node whose children we are getting.
   * @return A query object with the clustered children of node nid.
   */
  /*function quickdraw_navmenu_query_clusters($nid) {
    return db_query("SELECT c.id AS cluster_id, c.name AS cluster_name, d.cluster_order, count(*) as number
                FROM {quickdraw_navmenu_instance_data} d
                INNER JOIN {quickdraw_navmenu_cluster_type} c
                ON d.cluster_type = c.id
                WHERE d.parent = ':nid'
                GROUP BY cluster_type, cluster_order
                ORDER BY cluster_order", array(":nid" => $nid));
  }*/

  /**
   * Count the number of clustered children of a given type that are children of a node.
   *
   * @param nid The node whose children we are counting.
   * @param cluster The kind of cluster we want to count.
   * @return The number of clustered children of the node nid with the type 'cluster'.
   */
  /*function quickdraw_navmenu_count_cluster_children($nid, $cluster) {
    //some more fail here
    if ($cluster->number == 1) {
      $count = db_query("SELECT count(DISTINCT qd1.cluster_type) as number
                FROM quickdraw_navmenu_instance_data qd1
                JOIN quickdraw_navmenu_instance_data qd2 ON (qd1.parent = qd2.part)
                WHERE qd2.parent=':nid' AND qd2.cluster_type=':clusid' AND qd2.title=''",
                array(":nid" => $nid, ":clusid" => $cluster->cluster_id))->fetchObject();
      if ($count->number > 0) {
        return $count->number;
      }
    }
    return $cluster->number;
  }*/

  /**
   * Make a database query to get the clustered children of a given type for some node.
   *
   * @param nid The node whose children we are getting.
   * @param cluster_id The kind of clusters we will return.
   * @return A query object with the clustered children of node nid that have the type
   *         given by cluster_id.
   */
  /*function quickdraw_navmenu_get_clustered_nodes($nid, $cluster_id) {
    return db_query(
      "SELECT part, title, relationship_type, relationship_order, instance_order
       FROM {quickdraw_navmenu_instance_data}
       WHERE cluster_type = ':clusid' AND parent = ':nid'
       ORDER BY relationship_order, instance_order", array(":clusid" => $cluster_id, ":nid" => $nid));
  }*/

  /**
   * Generate HTML markup for the clustered children of a given node.
   *
   * @param parent_html_id The HTML id of the node's parent.
   * @param nid The node id of the node whose children we are working with.
   * @return HTML markup for the clustered children of the node nid.
   */
  /*function quickdraw_navmenu_make_clusters($parent_html_id, $nid) {
    $list = '';

    // ask db for the clustered children
    $result = quickdraw_navmenu_query_clusters($nid);
    foreach( $result as $row) {
        $cluster_id = $row->cluster_id;
        $cluster_name = $row->cluster_name;
        $number = quickdraw_navmenu_count_cluster_children($nid, $row);

        // produce the html id for the cluster we are working on
        $cluster_html_id = $parent_html_id . 'r' . $cluster_id;
        // generate the actual element that will contain the child
        $list .= '<li id="' . $cluster_html_id . '"><label><span class="aggtype">' . $cluster_name
                  . ' (' . $number . ')</span></label><ul>';
        $clustered = quickdraw_navmenu_get_clustered_nodes($nid, $cluster_id);
        foreach ($clustered as $part) {
          //if there are subclusters make them
          if ($part->title == '') {
            // fixme: this doesn't seem to happen in our data
            $list .= quickdraw_navmenu_make_clusters($cluster_html_id, $part->part);
          }
          //if not make the list items
          else {
            $list .= quickdraw_navmenu_make_li($parent_html_id, $cluster_id, $part->part, $part->title, $part->relationship_type, TRUE);
          }
        }
        $list .= '</ul></li>';
    }
    return $list;
  }*/

  /**
   * Create HTML markup for the piece of text that displays nodes that refer to
   * this resource statically.
   *
   * @param nid The node we are checking for references of.
   * @return HTML markup display the other nodes that reference this one.
   */
  function quickdraw_navmenu_make_referring($nid) {
    $html = '<br/><br/><h2 class="title block-title pngfix">Referring Resources</h2><ul id="realtree">';
    $html .= 'Not implemented currently';
    $html .= '</ul>';
    return $html;
  }

  /**
   * Get proper social nav image for a given node id as HTML markup.
   *
   * @param nid The node id we are checking.
   * @return HTML markup for the social nav image for the node with id nid.
   */
  function quickdraw_navmenu_social_nav($nid) {
    $social = FALSE; //set this to false to build tree without social cues
    if ($social) {
      $stats = statistics_get($nid);
      $count = $stats['totalcount'];
      if ($count>100) {
        $return_text = quickdraw_navmenu_social_img("g6.png", "red", "large", ">100");
      }
      elseif ($count>70) {
        $return_text = quickdraw_navmenu_social_img("g5.png", "purple", "moderate", ">70");
      }
      elseif ($count>30) {
        $return_text = quickdraw_navmenu_social_img("g4.png", "blue", "moderate", ">30");
      }
      elseif ($count>10) {
        $return_text = quickdraw_navmenu_social_img("g3.png", "green", "minimal", ">10");
      }
      elseif ($count>0) {
        $return_text = quickdraw_navmenu_social_img("g2.png", "green", "minimal", ">1");
      }
      else {
        $return_text = quickdraw_navmenu_social_img("g1.png", "white", "minimal", "0");
      }

      return $return_text;
    }
    else {
      return '';
    }
  }

  /**
   * Generate HTML markup for the social_nav image.
   *
   * @return HTML markup for the social_nav image.
   */
  function quickdraw_navmenu_social_img($filename, $color, $description, $number) {
    return "<img src='/" . drupal_get_path('module', 'quickdraw_navmenu')
         . '/' . $filename
         . "' title='This $color shade indicates a $description amount of"
         . " click traffic ($number clicks).'/>";
  }
