/**
 * @file
 * Contains javascript used to generate the navigation bar.
 */

(function ($)
{
  $(document).ready(function () {
  	// get node id of current page
  	function get_current_node_id() {
      // get the current node number
      // by checking after the last slash,
      // but cutting off before hitting a # or ?
      var hashes = window.location.href;
      var current = hashes.split('/')[hashes.split('/').length - 1];
  	  current = current.split('?')[0];
      current = current.split('#')[0];
  	  return current;
	  }

    // the jstree setup for the normal navmenu
    $("#quickdraw_navmenu_ajax_tree").jstree({
      "core": {
        "animation": 100,
      },
      "cookies": {
        "path": "/",
        "auto_save": "true",
        "save_opened": "quickdraw_navmenu_state",
      },
      "plugins": [ "themes", "html_data", "cookies", ],
      "html_data" : {
        "ajax": {
          // how the ajax calls should get the tree info
          "url": function(n) {
        		var url = '/quickdraw/navmenu/';
          		if(n === -1) { // on initial load
                var nid = get_current_node_id();
                if(nid == '') {
                  url = '/quickdraw/error';
                } else {
                  url += 'root/n' + get_current_node_id();
                }
          		} else { // when a tree node is expanded
          		  url += n.attr('id');
          		}
              console.log('trying ' + url);
              return url;
          },
          "success": function(data, textStatus, jqXHR) {
            // color the node we are currently on red
            // only works after something is done with the tree
            // unfortunately
            $(".u" + get_current_node_id()).css('color', 'red');
          },
        }
      },
      "themes": {
        "theme": "classic",
      },
    });
  });
})(jQuery);

// generates the preview tree with the given html_id
// as the root
function quickdraw_navmenu_get_initial_preview_tree(html_id) {
  var the_url = '/quickdraw/navmenu/root/' + html_id;
  // make an ajax call to get the root of the tree to display
  $.ajax({ 
    url: the_url
  })
  // once that is done, style it up with jstree like normal
  .done(function( data ) {
    $('#quickdraw_navmenu_ajax_preview_tree').html(data);
    $('#quickdraw_navmenu_ajax_preview_tree').jstree({
      "core": {
        "animation": 100,
      },
      "plugins": [ "themes", "html_data", ],
      "html_data" : {
        "ajax": {
          "url": function(n) {
            if(n === -1) {
              return '/quickdraw/navmenu/root/' + html_id;
            }
            else {
              return '/quickdraw/navmenu/' + n.attr('id');
            }
          },
        }
      },
      "themes": {
        "theme": "classic",
      },
    });
  });
}
